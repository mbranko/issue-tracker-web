# Issue Tracker - Front End

Project represents front end server for an issue tracker application.

It is developed as a demonstration for my thesis at the Faculty of Technical Sciencies in Novi Sad, Serbia 2018.

## How to run

To run server this requirements should be met:
- Node.js installed (8.12.0 LTS)
- TCP port 8080 should be free

Command to run server is:
```bash
npm install
npm run serve -- -port 8080
```
## About me

My name is Igor Gere. During my early age I developed interest in programming which from then has just evolved in to interest of computer science.

While attending Faculty of Technical Sciencies I got interested in web based applications. With over 10 years of professional development experience in the field of web applications I consider myself a true full stack developer.
