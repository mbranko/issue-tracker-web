import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/login', component: () => import('./views/AuthLogin.vue') },
    { path: '/logout', component: () => import('./views/AuthLogout.vue') },

    { path: '/', redirect: '/projects' },
    { path: '/projects', component: () => import('./views/Projects.vue') },

    { path: '/projects/:pid', component: () => import('./views/Project.vue'), props: true },
    { path: '/projects/:pid/edit', component: () => import('./views/ProjectForm.vue'), props: true, meta: { roles: ["ADMIN"] } },
    { path: '/projects/new/edit', component: () => import('./views/ProjectForm.vue'), props: true, meta: { roles: ["ADMIN"] } },

    { path: '/projects/:pid/issues/:iid', component: () => import('./views/Issue.vue'), props: true },
    { path: '/projects/:pid/issues/:iid/edit', component: () => import('./views/IssueForm.vue'), props: true, meta: { roles: ["ADMIN","USER"] } },
    { path: '/projects/:pid/issues/new/edit', component: () => import('./views/IssueForm.vue'), props: true, meta: { roles: ["ADMIN","USER"] } },

    { path: '/projects/:pid/issues/:iid/comments/:cid/edit', component: () => import('./views/CommentForm.vue'), props: true, meta: { roles: ["ADMIN","USER"] } },
    { path: '/projects/:pid/issues/:iid/comments/new/edit', component: () => import('./views/CommentForm.vue'), props: true, meta: { roles: ["ADMIN","USER"] } },

    { path: '/users', component: () => import('./views/Users.vue') },
    { path: '/users/:uid/edit', component: () => import('./views/UserForm.vue'), props: true, meta: { roles: ["ADMIN"] } },
    { path: '/users/new/edit', component: () => import('./views/UserForm.vue'), props: true, meta: { roles: ["ADMIN"] } },
    { path: '/**', redirect: '/' }
  ]
});

// Role based route checks
router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.roles && to.meta.roles.indexOf(localStorage.role) == -1) {
    next({
        path: '/',
      });
  } else {
    next();
  }
});

export default router;
