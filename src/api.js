import axios from 'axios'
import router from '@/router'

export const api = axios.create({
  baseURL: 'http://localhost:8090/api',
  validateStatus: detectAuthProblem,
  auth: {
    username: localStorage.username,
    password: localStorage.password
  }
})

function detectAuthProblem(status) {
  if (![401, 403].indexOf(status)) {
    router.push('/login');
  }

  return true;
}

export function hasError(errors, field) {
  for (let e in errors) {
    if (errors[e].field == field) {
      return true;
    }
  }

  return false;
}

export function getError(errors, field) {
  let message = '';
  for (let e in errors) {
    if (errors[e].field == field) {
      if (message != '') {
        message+=', ';
      }

      message += errors[e].message;
    }
  }
  return message;
}
